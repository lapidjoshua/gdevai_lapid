﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;


public class Graph : MonoBehaviour {
    [SerializeField]
    protected List<Node> nodes = new List<Node>();

    public virtual List<Node> getNodes
    {
        get
        {
            return nodes;
        }
    }

    public virtual Path GetDijkstraPath(Node start, Node end)
    {
        //We don't want null arguments
        if (start == null || end == null)
        {
            throw new ArgumentNullException();
        }

        Path path = new Path();

        // If the start and end nodes are the same, we can return return the start node

        if (start == end)
        {
            path.getNodes.Add(start);
            return path;
        }

        List<Node> unvisited = new List<Node>();

        // Previous Nodes in optimal path from source
        Dictionary<Node, Node> previous = new Dictionary<Node, Node>();

        // The calculated distances 
        Dictionary<Node, float> distances = new Dictionary<Node, float>();

        //set all nodes to infinity except start node
        for (int i = 0; i < nodes.Count; i++)
        {
            Node node = nodes[i];
            unvisited.Add(node);

            //setting the node distance to infinity 
            distances.Add(node, float.MaxValue);
        }

        //set the starting node distance to 0
        distances[start] = 0f;

        while (unvisited.Count != 0)
        {
            // Sort the unvisited list by distance, smallest distance at start and largest at end
            unvisited = unvisited.OrderBy(node => distances[node]).ToList();

            //get the node with the smallest distance
            Node current = unvisited[0];

            //remove the current node from the unvisited list
            unvisited.Remove(current);

            //when the current node is equal to the end node, then we can break and return the path
            if (current == end)
            {
                //construct the shortest path
                while (previous.ContainsKey(current))
                {
                    //insert the node unto the final result
                    path.getNodes.Insert(0, current);

                    //traverse from start to end
                    current = previous[current];
                }

                //insert the source unto the final result
                path.getNodes.Insert(0, current);
                break;
            }

            // looping through the node connections (neighbors and where the connection is available at unvisited lits

            for (int i = 0; i < current.getConnections.Count; i++)
            {
                Node neighbor = current.getConnections[i];

                // getting the distance between the curent node and the connection

                float length = Vector3.Distance(current.transform.position, neighbor.transform.position);

                //the distance from start node to this connection of current node
                float alt = distances[current] + length;

                if (alt < distances[neighbor])
                {
                    distances[neighbor] = alt;
                    previous[neighbor] = current;
                }
            }   
        }

        path.Bake();
        return path;
    }
}
