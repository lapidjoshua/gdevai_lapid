﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Player : MonoBehaviour {

    public int playerHealth;
    public Text playerHealthText;
	
	void Update () {

        playerHealthText.text = playerHealth.ToString();

        if (playerHealth <= 0)
        {
            Application.Quit();
        }
	}
}
