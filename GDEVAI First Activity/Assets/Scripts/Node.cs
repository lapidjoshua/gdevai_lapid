﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Node : MonoBehaviour {

    [SerializeField]
    protected List<Node> connections = new List<Node>();

    public virtual List<Node> getConnections
    {
        get
        {
            return connections;
        }
    }

    public Node this [int index]
    {
        get
        {
            return connections[index];
        }
    }

    private void OnValidate()
    {
        //Remove Duplicate Elements
        {
            connections = connections.Distinct().ToList();
        }
    }
}
