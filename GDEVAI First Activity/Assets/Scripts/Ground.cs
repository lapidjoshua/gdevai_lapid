﻿using UnityEngine;

public class Terrain : MonoBehaviour {

    public Color hoverColor;
    private Renderer rend;
    private Color startColor;

    private void Start()
    {

        rend = GetComponent<Renderer>();
    }
    private void OnMouseEnter()
    {
       rend.material.color = hoverColor;
    }

    private void OnMouseExit()
    {
        rend.material.color = startColor;
    }
}
