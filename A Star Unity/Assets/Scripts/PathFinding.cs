﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathFinding : MonoBehaviour
{


    public Grid grid;
    public Transform startPos, targetPos;

    private void Awake()
    {

        grid = GetComponent<Grid>();

    }
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void LateUpdate()
    {
        FindPath(startPos.position, targetPos.position);       
    }

    void FindPath(Vector3 start, Vector3 target)
    {
        Node startNode = grid.NodeFromWorldPoint(start);
        Node targetNode = grid.NodeFromWorldPoint(target);

        List<Node> openNodes = new List<Node>();
        HashSet<Node> closedNodes = new HashSet<Node>();

        openNodes.Add(startNode); // add the start node to the open list to behin the algorithm 
        while (openNodes.Count > 0)
        {
            Node currentNode = openNodes[0]; // create a node and set it to the first item in the open nodes

            // loop through the open list starting from the second object
            for (int i = 1; i < openNodes.Count; i++)
            {
                if (openNodes[i].F_Cost < currentNode.F_Cost ||
                    openNodes[i].F_Cost == currentNode.F_Cost &&
                    openNodes[i].H_Cost < currentNode.H_Cost)
                {
                    currentNode = openNodes[i]; // set the current node to that object
                }
            }

            openNodes.Remove(currentNode);
            closedNodes.Add(currentNode);

            if (currentNode == targetNode)
            {
                GetFinalPath(startNode, targetNode);
            }

            //obstacle detection

            foreach (Node neighbor in grid.GetNeighboringNodes(currentNode))
            {
                if (!neighbor.IsWall || closedNodes.Contains(neighbor))
                {
                    continue; //skip node
                }

                int moveCost = currentNode.G_Cost + GetManhattanDistance(currentNode, neighbor);

                if (moveCost < neighbor.G_Cost || !openNodes.Contains(neighbor))
                {
                    neighbor.G_Cost = moveCost;
                    neighbor.H_Cost = GetManhattanDistance(neighbor, targetNode);
                    neighbor.ParentNode = currentNode;
                    if (!openNodes.Contains(neighbor))
                    {
                        openNodes.Add(neighbor);
                    }                 
                }                
            }
        }
    }

    void GetFinalPath(Node start, Node end)
    {
        List<Node> finalPath = new List<Node>();
        Node currentNode = end;

        while (currentNode != start)
        {
            finalPath.Add(currentNode);
            currentNode = currentNode.ParentNode;
        }

        finalPath.Reverse();
        grid.FinalPath = finalPath;

    }

    int GetManhattanDistance(Node nodeA, Node nodeB)
    {
        int x = Mathf.Abs(nodeA.GridPosX - nodeB.GridPosX); //x1-x2
        int y = Mathf.Abs(nodeA.GridPosY - nodeB.GridPosY); //y1-y2

        return x + y;
    }
}
