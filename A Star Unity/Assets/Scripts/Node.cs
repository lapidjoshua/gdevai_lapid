﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node{

    /*  */
    public int GridPosX;
    public int GridPosY;
    /* */
    public bool IsWall;

    /* The World Position of the node */
    public Vector3 Position;

    /* Stores the previous node that it cam from so we can trace he shortest path */
    public Node ParentNode;

    /* The cost of moving to the next node */
    public int G_Cost;

    public int H_Cost;
    /* Quick getter function to add G cost and cost
     * 
     * A* formula = 
     * 
     * F(N) = G(N) + H(N)
     */
    public int F_Cost
    {
        get
        {
            return G_Cost + H_Cost;
        }
    }

    public Node(bool isWall, Vector3 worldPos, int gridX, int gridY)
    {
        this.IsWall = isWall;
        this.Position = worldPos;
        this.GridPosX = gridX;
        this.GridPosY = gridY;
    }
    
}
