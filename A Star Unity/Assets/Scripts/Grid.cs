﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grid : MonoBehaviour {

    /* this is where the algorithm will start the pathFinding from */

    public Transform StartPosition;

    /* This is the mask that the algorithm will look for when trying to find obstruction to the path*/

    public LayerMask WallMask;

    /* A Vector 2 to store the width and hieght of the graph in world units*/

    public Vector2 GridWorldSize = new Vector2(30, 30);

    /* Determines how big each node will be*/

    public float NodeRadius = 0.5f;

    /* The Distance the nodes will spawn from each other*/

    public float DistanceBetweenNodes = 0;

    /*the array of Nodes */

    public static Node[,] NodeArray;

    /* the completed path */

    public List<Node> FinalPath;

    /**
     * Twice the amount of node radius
     */
    public float NodeDiameter;

    /**
     * Size of the grid array in units
     */

    public static int GridSizeX, GridSizeY;

    private void Start()
    {
        NodeDiameter = NodeRadius * 2;

        //divide the grids world co-ordinates by the diameter to gt the size of the fraph in array units
        GridSizeX = Mathf.RoundToInt(GridWorldSize.x / NodeDiameter);
        GridSizeY = Mathf.RoundToInt(GridWorldSize.y / NodeDiameter);
        NodeArray = new Node[GridSizeX, GridSizeY];

        InvokeRepeating("MoveFollower", 0, 0.30f);
    }

    private void Update()
    {
        CreateGrid();        
    }

    private void CreateGrid()
    {
        
        Vector3 bottomLeft = transform.position - Vector3.right * GridWorldSize.x / 2 - Vector3.forward * GridWorldSize.y / 2;

        for (int x = 0; x < GridSizeX; x++) //iterate through first dimension
        {
            for (int y = 0; y < GridSizeY; y++)//iterate through second dimension
            {
                Vector3 worldPoint = bottomLeft + Vector3.right * (x * NodeDiameter + NodeRadius) + Vector3.forward * (y * NodeDiameter + NodeRadius);

                bool isWall = true;

                if (Physics.CheckSphere(worldPoint, NodeRadius, WallMask))
                {
                    isWall = false;
                }

                NodeArray[x, y] = new Node(isWall, worldPoint, x, y);
            }
        }
    }

    /**
     * Gets tge neighboring node of the given node
     */

    public List<Node> GetNeighboringNodes(Node neighborNode)
    {
        List<Node> neighborList = new List<Node>();
        int checkX, checkY;

        //check the right side of the current node
        checkX = neighborNode.GridPosX + 1;
        checkY = neighborNode.GridPosY;

        if (checkX >= 0 && checkX < GridSizeX) //check x 
        {
            if (checkY >= 0 && checkY < GridSizeY)
            {
                neighborList.Add(NodeArray[checkX, checkY]);
            }
        }

        // check the left side of the current node

        checkX = neighborNode.GridPosX - 1;
        checkY = neighborNode.GridPosY;

        if (checkX >= 0 && checkX < GridSizeX)
        {
            if (checkY >= 0 && checkY < GridSizeY)
            {
                neighborList.Add(NodeArray[checkX, checkY]);
            }
        }

        //checks the top side of the curent node

        checkX = neighborNode.GridPosX;
        checkY = neighborNode.GridPosY + 1;

        if (checkX >= 0 && checkX < GridSizeX)
        {
            if (checkY >= 0 && checkY < GridSizeY)
            {
                neighborList.Add(NodeArray[checkX, checkY]);
            }
        }

        //check the bottom side of the current node

        checkX = neighborNode.GridPosX;
        checkY = neighborNode.GridPosY - 1;

        if (checkX >= 0 && checkX < GridSizeX)
        {
            if (checkY >= 0 && checkY < GridSizeY)
            {
                neighborList.Add(NodeArray[checkX, checkY]);
            }
        }

        return neighborList;
    }

    /**
     * Gets the closest node to the given world position
     */
    public Node NodeFromWorldPoint(Vector3 worldPos)
    {
        float xPos = ((worldPos.x + GridWorldSize.x / 2) / GridWorldSize.x);

        float yPos = ((worldPos.z + GridWorldSize.y / 2) / GridWorldSize.y);

        xPos = Mathf.Clamp01(xPos);
        yPos = Mathf.Clamp01(yPos);

        int x = Mathf.RoundToInt((GridSizeX - 1) * xPos);
        int y = Mathf.RoundToInt((GridSizeY - 1) * yPos);

        return NodeArray[x, y];
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireCube(transform.position, new Vector3(GridWorldSize.x, 1, GridWorldSize.y));

        if (NodeArray != null)
        {
            foreach (Node n in NodeArray)
            {
                if (n.IsWall)
                {
                    Gizmos.color = Color.white;
                }

                else
                {
                    Gizmos.color = Color.yellow;
                }

                if (FinalPath != null)
                {
                    if (FinalPath.Contains(n))
                    {
                        Gizmos.color = Color.red;
                    }
                }

                Gizmos.DrawCube(n.Position, Vector3.one * (NodeDiameter - DistanceBetweenNodes));
            }

            
        }
    }

    void MoveFollower()
    {
        StartPosition.transform.position = new Vector3(FinalPath[0].Position.x, 0, FinalPath[0].Position.z);
    }
}
